﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>



<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     
    <script>
  $( function() {
    var comuniComplet = 
    $( "#tags" ).autocomplete({
      source: availableTags
    });
  } );
  </script>

    <div class="jumbotron">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br /> <br />
        <br /> <br />
        &nbsp;<br />
        <table style="width: 100%;">
            <tr>
                <td style="width: 200px">&nbsp;</td>
                <td style="width: 613px">
                    <asp:Label ID="CfLabel" runat="server"></asp:Label>
                </td>
                <td style="width: 613px">&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 200px">Nome&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td style="width: 613px">
        <asp:TextBox ID="NameBox" runat="server" ToolTip="Inserisci il nome e l'eventuale secondo nome" ValidationGroup="DataValidation"></asp:TextBox>
                    
                </td>
                <td style="width: 613px"><asp:RequiredFieldValidator ID="OnlyAlpha" runat="server" ControlToValidate="NameBox"
    ValidationExpression="^[a-zA-Z]+$" ErrorMessage="*Valid characters: Alphabets and space." /></td>
            </tr>
            <tr>
                <td style="height: 46px; width: 200px">Cognome</td>
                <td style="width: 613px; height: 46px"><asp:TextBox ID="SurnameBox" runat="server" ToolTip="Inserisci il cognome" ValidationGroup="DataValidation"></asp:TextBox>
                </td>
                <td style="width: 613px; height: 46px">&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 200px; height: 46px;">Genere:</td>
                <td style="width: 613px; height: 46px;">
        <asp:RadioButton ID="MaleBtn" runat="server" Text="Maschile" OnCheckedChanged="MaleBtn_CheckedChanged" TextAlign="Left" Checked="True" GroupName="Gender" />
        <asp:RadioButton ID="FemaleBtn" runat="server" Text="Femminile" OnCheckedChanged="FemaleBtn_CheckedChanged" TextAlign="Left" GroupName="Gender" />
                </td>
                <td style="width: 613px; height: 46px;"></td>
            </tr>
            <tr>
                <td style="width: 200px">Data di Nascita
    </td>
                <td style="width: 613px">
                    <asp:DropDownList ID="dayList" runat="server">
                        <asp:ListItem Value="1"></asp:ListItem>
                        <asp:ListItem Value="2"></asp:ListItem>
                        <asp:ListItem Value="3"></asp:ListItem>
                        <asp:ListItem Value="4"></asp:ListItem>
                        <asp:ListItem Value="5"></asp:ListItem>
                        <asp:ListItem Value="6"></asp:ListItem>
                        <asp:ListItem Value="7"></asp:ListItem>
                        <asp:ListItem Value="8"></asp:ListItem>
                        <asp:ListItem Value="9"></asp:ListItem>
                        <asp:ListItem Value="10"></asp:ListItem>
                        <asp:ListItem Value="11"></asp:ListItem>
                        <asp:ListItem Value="12"></asp:ListItem>
                        <asp:ListItem Value="13"></asp:ListItem>
                        <asp:ListItem Value="14"></asp:ListItem>
                        <asp:ListItem Value="15"></asp:ListItem>
                        <asp:ListItem Value="16"></asp:ListItem>
                        <asp:ListItem Value="17"></asp:ListItem>
                        <asp:ListItem Value="18"></asp:ListItem>
                        <asp:ListItem Value="19"></asp:ListItem>
                        <asp:ListItem Value="20"></asp:ListItem>
                        <asp:ListItem Value="21"></asp:ListItem>
                        <asp:ListItem Value="22"></asp:ListItem>
                        <asp:ListItem Value="23"></asp:ListItem>
                        <asp:ListItem Value="24"></asp:ListItem>
                        <asp:ListItem Value="25"></asp:ListItem>
                        <asp:ListItem Value="26"></asp:ListItem>
                        <asp:ListItem Value="27"></asp:ListItem>
                        <asp:ListItem Value="28"></asp:ListItem>
                        <asp:ListItem Value="29"></asp:ListItem>
                        <asp:ListItem Value="30"></asp:ListItem>
                        <asp:ListItem Value="31"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="monthList" runat="server">
                        <asp:ListItem Value="1">Gennaio</asp:ListItem>
                        <asp:ListItem Value="2">Febbraio</asp:ListItem>
                        <asp:ListItem Value="3">Marzo</asp:ListItem>
                        <asp:ListItem Value="4">Aprile</asp:ListItem>
                        <asp:ListItem Value="5">Maggio</asp:ListItem>
                        <asp:ListItem Value="6">Giugno</asp:ListItem>
                        <asp:ListItem Value="7">Luglio</asp:ListItem>
                        <asp:ListItem Value="8">Agosto</asp:ListItem>
                        <asp:ListItem Value="9">Settembre</asp:ListItem>
                        <asp:ListItem Value="10">Ottobre</asp:ListItem>
                        <asp:ListItem Value="11">Novembre</asp:ListItem>
                        <asp:ListItem Value="12">Dicembre</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="yearBox" runat="server" MaxLength="4" type="number" OnTextChanged="yearBox_TextChanged" ValidationGroup="DataValidation"></asp:TextBox>
                </td>
                <td style="width: 613px">&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 200px">Luogo di nascita</td>
                <td style="width: 613px">
                    <asp:DropDownList ID="BirthPlaceList" runat="server"  OnSelectedIndexChanged="BirthPlaceList_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td style="width: 613px">&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 200px">&nbsp;</td>
                <td style="width: 613px">
                    <asp:Button ID="CalcBtn" runat="server" OnClick="CalcBtn_Click" Text="Calcola Codice Fiscale" ValidationGroup="DataValidation" />
                </td>
                <td style="width: 613px">&nbsp;</td>
            </tr>
        </table>
    </div>
       
</asp:Content>

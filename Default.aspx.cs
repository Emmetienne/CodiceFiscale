﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;

public partial class _Default : Page
{

    CodF codFisc = new CodF();
    List<string> comuniList = new List<string>();
    List<string> codList = new List<string>();

    View v = new View();

    string[] codArray;
    static int cityIndex;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            ReadCsv();
            BirthPlaceList.SelectedIndex = cityIndex;


        }
     


    }

    protected void ReadCsv()
    {

        //    File.ReadAllText(Server.MapPath("~/App_Data/Details.txt"));
        if (!Page.IsPostBack)
        {


            //DataTable table = new DataTable();
            //table.Columns.Add("Comune");
            //table.Columns.Add("Codice");




            int i = 0;
            using (var reader = new StreamReader(Server.MapPath("~") + @"/App_Data/comuni.csv"))
            {



                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    Console.WriteLine(values);
                    //comuniList.Add(values[0]);
                    //codList.Add(values[1]);

                    //DataRow dr = table.NewRow();
                    //dr["Comune"] = values[0];
                    //dr["Codice"] = values[1];
                    //table.Rows.Add(dr);
                    BirthPlaceList.Items.Add(new ListItem(values[0], values[1]));

                    i++;
                }
            }
            //   ddlBirthYear.Items.Add(new ListItem("text", "value"));
            Console.WriteLine(comuniList.Count);
            Console.WriteLine(codList.Count);



            // BirthPlaceList.DataSource = table;
            //BirthPlaceList.DataSource = comuniList;

            //  BirthPlaceList.DataBind();

        }

    }

    protected void CalcBtn_Click(object sender, EventArgs e)
    {


        string buf = NameBox.Text;
        NameBox.Text = buf.ToUpper();


        buf = SurnameBox.Text;
        SurnameBox.Text = buf.ToUpper();

        bool male = false;
        if (MaleBtn.Checked == true)
        {
            male = true;
        }



        string cityString = "";
        cityString = BirthPlaceList.SelectedValue;
        
        
       
        


        CfLabel.Text = codFisc.GenerateCf(NameBox.Text, SurnameBox.Text, male,
            byte.Parse(dayList.SelectedValue), byte.Parse(monthList.SelectedValue),
            yearBox.Text, cityString);








    }

    protected void yearBox_TextChanged(object sender, EventArgs e)
    {


        if (yearBox.Text.All(Char.IsNumber))
        {
            if (yearBox.Text.Length == 4)
            {
                CalcBtn.Enabled = true;


            }
            else
            {
                CalcBtn.Enabled = false;
            }


        }
        else
        {

            CalcBtn.Enabled = false;
        }



    }


    protected void MaleBtn_CheckedChanged(object sender, EventArgs e)
    {
        if (FemaleBtn.Checked == true)
        {

            FemaleBtn.Checked = false;

        }
    }

    protected void FemaleBtn_CheckedChanged(object sender, EventArgs e)
    {
        if (MaleBtn.Checked == true)
        {

            MaleBtn.Checked = false;

        }
    }



    protected void BirthPlaceList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            cityIndex = BirthPlaceList.SelectedIndex;
         //   CityBox.Text = BirthPlaceList.SelectedValue;

        }
        
        

        //  CfLabel.Text = cityIndex.ToString();


    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Descrizione di riepilogo per CodF
/// </summary>
public partial class CodF
{
    string name;
    string surname;
    bool male;
    string date;
    string place;

    string resultCf;

    DataTable evenTable;
    DataTable oddTable;
    DataTable finalTable;


    public CodF()
    {
        //
        // TODO: aggiungere qui la logica del costruttore
        //
    }

    public string GenerateCf(string n, string s, bool g, byte d, byte m, string y, string p)
    {

        // resultCf = SurnameCode(s) + NameCode(n);

        resultCf = SurnameCode(s).ToLower() + NameCode(n).ToLower() + YearCode(y) + MonthCode(m) + GenderCode(g,d) + p ;
        resultCf.ToString().ToUpper();

        string evenString = "";
        evenString += resultCf[1];
        evenString += resultCf[3];
        evenString += resultCf[5];
        evenString += resultCf[7];
        evenString += resultCf[9];
        evenString += resultCf[11];
        evenString += resultCf[13];
        evenString.ToUpper();
        //evenString += resultCf[1];
        //+ resultCf[3] + resultCf[5] + resultCf[7] + resultCf[9] + resultCf[11] + resultCf[13];

        string oddString = "";
        oddString = resultCf[0].ToString() + resultCf[2] + resultCf[4] + resultCf[6] + resultCf[8] + resultCf[10] + resultCf[12] + resultCf[14];

        resultCf += ControlCode(evenString, oddString);

        return resultCf.ToUpper();

    }

    public string SurnameCode(string s)
    {
        Tuple<int, string, string> c;
        c = CountConsonant(s);

        string surCode = "";

        if (s.Length < 3)
        {

            if (c.Item1 == 2)
            {

                surCode = c.Item3[0] + c.Item2[0] + "X";

            }

           else if (c.Item1 == 1)
            {

                surCode = c.Item3[0] + "X" + "X";

            }

           if (c.Item1 == 0)
           {
                if (c.Item2.Length == 2)
                {
                    surCode = c.Item2[0] + c.Item2[0] + "X";
                }
                else if (c.Item2.Length == 1)
                    {

                    surCode = c.Item2[0] + "X" + "X";

                }
           }
            return surCode;

        }
        else
        {
            


            if (c.Item1 >= 3)
            {

                surCode = c.Item3[0].ToString() + c.Item3[1].ToString() + c.Item3[2].ToString();


            }

            else if (c.Item1 >= 2)
            {

                surCode = c.Item3[0].ToString() + c.Item3[1].ToString() + c.Item2[0].ToString();


            }

            else if (c.Item1 >= 1)
            {

                surCode = c.Item3[0].ToString() + c.Item2[0].ToString() + c.Item2[1].ToString();


            }
            else if (c.Item1 >= 0)
            {

                surCode = c.Item2[0].ToString() + c.Item2[1].ToString() + c.Item2[2].ToString();


            }



            return surCode;
        }




    }

    public string NameCode(string n)
    {

        string nameCode = "";

        if (n.Length < 3)
        {

            return n + "X";



        }
        else
        {
            Tuple<int, string, string> c;
            c = CountConsonant(n);



            if (c.Item1 > 3)
            {

                nameCode = c.Item3[0].ToString() + c.Item3[2].ToString() + c.Item3[3].ToString();


            }
            else
            {

                if (c.Item1 == 3)
                {

                    nameCode = c.Item3[0].ToString() + c.Item3[1].ToString() + c.Item3[2].ToString();


                }

                else if (c.Item1 == 2)
                {

                    nameCode = c.Item3[0].ToString() + c.Item3[1].ToString() + c.Item2[0].ToString();


                }

                else if (c.Item1 == 1)
                {

                    nameCode = c.Item3[0].ToString() + c.Item2[0].ToString() + c.Item2[1].ToString();


                }
                else if (c.Item1 == 0)
                {

                    nameCode = c.Item2[0].ToString() + c.Item2[1].ToString() + c.Item2[2].ToString();


                }

            }

            return nameCode;
        }




    }

    public Tuple<int, string, string> CountConsonant(string c)
    {
        int count = 0;
        string consonants = "";
        string vowels = "";
        char[] vowelsTest = new char[] { 'a', 'e', 'i', 'o', 'u', ' ', 'A', 'E', 'I', 'O', 'U' };

        for (int i = 0; i < c.Length; i++)
        {
            if (!vowelsTest.Contains(c[i]))
            {
                consonants += c[i];
                count++;

            }
            else
            {
                vowels += c[i];

            }


        }

        return Tuple.Create(count, vowels, consonants);
    }


    public string YearCode(string y)
    {
        
        string year = "";
        year = y[2].ToString() + y[3].ToString();
        

        return year;
    }


    public string MonthCode(byte m)
    {
        string month = "";

        switch (m)
        {
            case 1:
                month = "A";
                    break;
            case 2:
                month = "B";
                break;
            case 3:
                month = "C";
                break;
            case 4:
                month = "D";
                break;
            case 5:
                month = "E";
                break;
            case 6:
                month = "H";
                break;
            case 7:
                month = "L";
                break;
            case 8:
                month = "M";
                break;
            case 9:
                month = "P";
                break;
            case 10:
                month = "R";
                break;
            case 11:
                month = "S";
                break;
            case 12:
                month = "T";
                break;

        }

        return month;

        
    }

    public string GenderCode(bool g, byte d)
    {
        string day;

        if (g == false)
        {
            day = (d + 40).ToString();


        }
        else
        {
            
            day = d.ToString();
            if (day.Length == 1)
            {
                day = "0" + day;

            }
        }

        return day.ToString();
    }

    public string ControlCode(string evenLetters, string oddLetters)
    {
        evenLetters.ToUpper();
        oddLetters.ToUpper();
        int evenChara = 0;
        int oddChara = 0;
        //calc the first number
        EvenTable();
        OddTable();

        //get control number from even character
        for (int i = 0; i < evenLetters.Length; i++)
        {
            string l = evenLetters[i].ToString();
            DataRow foundRow = evenTable.Rows.Find(l);

           

            if (foundRow != null)
            {
                Console.WriteLine(foundRow.ToString());

                string res = "";
                res = (foundRow.Field<string>("Risultato"));

                evenChara += int.Parse(res);
                    
                    //evenTable.Rows[0].ToString());
            }
            else
            {
                Console.WriteLine("A row with the primary key of " + l + " could not be found");
            }



        }


        for (int i = 0; i < oddLetters.Length; i++)
        {
            string l = oddLetters[i].ToString();
            DataRow foundRow = oddTable.Rows.Find(l);



            if (foundRow != null)
            {
                Console.WriteLine(foundRow.ToString());

                string res = "";
                res = (foundRow.Field<string>("Risultato"));

                oddChara += int.Parse(res);

                //evenTable.Rows[0].ToString());
            }
            else
            {
                Console.WriteLine("A row with the primary key of " + l + " could not be found");
            }



        }
        float sumChara = 0;
        sumChara = evenChara + oddChara;
        sumChara = sumChara % 26;
       

        //int resultChara = (int) sumChara;

        return ConvertControlNumber((int) sumChara);
    }

    public void EvenTable()
    {
        evenTable = new DataTable();
        DataColumn[] pKey = new DataColumn[1];
        DataColumn column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "Carattere";
        
        evenTable.Columns.Add(column);
        pKey[0] = column;


        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "Risultato";
        evenTable.Columns.Add(column);

    

        DataRow dr = evenTable.NewRow();
        dr["Carattere"] = 0;
        dr["Risultato"] = 0;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = 1;
        dr["Risultato"] = 1;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = 2;
        dr["Risultato"] = 2;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = 3;
        dr["Risultato"] = 3;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = 4;
        dr["Risultato"] = 4;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = 5;
        dr["Risultato"] = 5;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = 6;
        dr["Risultato"] = 6;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = 7;
        dr["Risultato"] = 7;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = 8;
        dr["Risultato"] = 8;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = 9;
        dr["Risultato"] = 9;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "A";
        dr["Risultato"] = 0;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "B";
        dr["Risultato"] = 1;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "C";
        dr["Risultato"] = 2;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "D";
        dr["Risultato"] = 3;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "E";
        dr["Risultato"] = 4;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "F";
        dr["Risultato"] = 5;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "G";
        dr["Risultato"] = 6;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "H";
        dr["Risultato"] = 7;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "I";
        dr["Risultato"] = 8;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "J";
        dr["Risultato"] = 9;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "K";
        dr["Risultato"] = 10;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "L";
        dr["Risultato"] = 11;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "M";
        dr["Risultato"] = 12;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "N";
        dr["Risultato"] = 13;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "O";
        dr["Risultato"] = 14;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "P";
        dr["Risultato"] = 15;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "Q";
        dr["Risultato"] = 16;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "R";
        dr["Risultato"] = 17;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "S";
        dr["Risultato"] = 18;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "T";
        dr["Risultato"] = 19;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "U";
        dr["Risultato"] = 20;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "V";
        dr["Risultato"] = 21;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "W";
        dr["Risultato"] = 22;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "X";
        dr["Risultato"] = 23;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "Y";
        dr["Risultato"] = 24;
        evenTable.Rows.Add(dr);

        dr = evenTable.NewRow();
        dr["Carattere"] = "Z";
        dr["Risultato"] = 25;
        evenTable.Rows.Add(dr);

        evenTable.PrimaryKey = pKey;

    }

    public void OddTable()
    {

        oddTable = new DataTable();
        DataColumn[] pKey = new DataColumn[1];
        DataColumn column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "Carattere";

        oddTable.Columns.Add(column);
        pKey[0] = column;


        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "Risultato";
        oddTable.Columns.Add(column);

      

        DataRow dr = oddTable.NewRow();
        dr["Carattere"] = 0;
        dr["Risultato"] = 1;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = 1;
        dr["Risultato"] = 0;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = 2;
        dr["Risultato"] = 5;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = 3;
        dr["Risultato"] = 7;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = 4;
        dr["Risultato"] = 9;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = 5;
        dr["Risultato"] = 13;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = 6;
        dr["Risultato"] = 15;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = 7;
        dr["Risultato"] = 17;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = 8;
        dr["Risultato"] = 19;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = 9;
        dr["Risultato"] = 21;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "A";
        dr["Risultato"] = 1;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "B";
        dr["Risultato"] = 0;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "C";
        dr["Risultato"] = 5;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "D";
        dr["Risultato"] = 7;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "E";
        dr["Risultato"] = 9;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "F";
        dr["Risultato"] = 13;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "G";
        dr["Risultato"] = 15;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "H";
        dr["Risultato"] = 17;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "I";
        dr["Risultato"] = 19;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "J";
        dr["Risultato"] = 21;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "K";
        dr["Risultato"] = 2;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "L";
        dr["Risultato"] = 4;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "M";
        dr["Risultato"] = 18;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "N";
        dr["Risultato"] = 20;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "O";
        dr["Risultato"] = 11;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "P";
        dr["Risultato"] = 3;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "Q";
        dr["Risultato"] = 6;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "R";
        dr["Risultato"] = 8;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "S";
        dr["Risultato"] = 12;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "T";
        dr["Risultato"] = 14;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "U";
        dr["Risultato"] = 16;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "V";
        dr["Risultato"] = 10;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "W";
        dr["Risultato"] = 22;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "X";
        dr["Risultato"] = 25;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "Y";
        dr["Risultato"] = 24;
        oddTable.Rows.Add(dr);

        dr = oddTable.NewRow();
        dr["Carattere"] = "Z";
        dr["Risultato"] = 23;
        oddTable.Rows.Add(dr);

        oddTable.PrimaryKey = pKey;

    }


    public string ConvertControlNumber(int c)
    {
        string chara = "";

        switch (c)
        {

            case 0:
                chara = "A";
                break;

            case 1:
                chara = "B";
                break;

            case 2:
                chara = "C";
                break;

            case 3:
                chara = "D";
                break;

            case 4:
                chara = "E";
                break;

            case 5:
                chara = "F";
                break;

            case 6:
                chara = "G";
                break;

            case 7:
                chara = "H";
                break;

            case 8:
                chara = "I";
                break;

            case 9:
                chara = "J";
                break;

            case 10:
                chara = "K";
                break;

            case 11:
                chara = "L";
                break;

            case 12:
                chara = "M";
                break;

            case 13:
                chara = "N";
                break;

            case 14:
                chara = "O";
                break;

            case 15:
                chara = "P";
                break;

            case 16:
                chara = "Q";
                break;

            case 17:
                chara = "R";
                break;

            case 18:
                chara = "S";
                break;

            case 19:
                chara = "T";
                break;

            case 20:
                chara = "U";
                break;

            case 21:
                chara = "V";
                break;

            case 22:
                chara = "W";
                break;

            case 23:
                chara = "X";
                break;

            case 24:
                chara = "Y";
                break;

            case 25:
                chara = "Z";
                break;


        }


        return chara;
    }

    public string Name
    {
        get
        {
            return name;
        }

        set
        {
            name = value;
        }
    }

    public string Surname
    {
        get
        {
            return surname;
        }

        set
        {
            surname = value;
        }
    }

    public bool Male
    {
        get
        {
            return male;
        }

        set
        {
            male = value;
        }
    }

    public string Date
    {
        get
        {
            return date;
        }

        set
        {
            date = value;
        }
    }

    public string Place
    {
        get
        {
            return place;
        }

        set
        {
            place = value;
        }
    }

    public string ResultCf
    {
        get
        {
            return resultCf;
        }

        set
        {
            resultCf = value;
        }
    }
}